<?php
function tentukan_nilai($number)
{
    if ($number >=85){
        return "Sangat Baik"; // Asumsi Maksimal tidak ada
    }
    else if ($number>=70){
        return "Baik"; // Tidak perlu kasus <85, karena kalau >=85 sudah direturn sehingga kasus ini tidak dijalankan
    }
    else if ($number >=60){
        return "Cukup"; // Tidak perlu kasus <70, karena kalau >=70 sudah direturn sehingga kasus ini tidak dijalankan
    }
    else{
        return "Kurang";// Tidak perlu kasus <60, karena kalau >=60 sudah direturn sehingga kasus ini tidak dijalankan
    }
}

//TEST CASES
echo tentukan_nilai(98) . "<br>"; //Sangat Baik
echo tentukan_nilai(76) . "<br>"; //Baik
echo tentukan_nilai(67) . "<br>"; //Cukup
echo tentukan_nilai(43) . "<br>"; //Kurang
?>