<?php
function ubah_huruf($string){
    // ord fungsi untuk ubah char ke ascii
    //chr fungsi ubah ascii ke char
    $data='';
    $array=str_split($string);
    foreach ($array as $value){
        if(ord($value)===90){//handle kasus Z
            $data.="A";    
        }
        else if (ord($value)===122){ //handle kasus z
            $data.="a";
        }
        else{
            $temp=ord($value)+1;
            $data.=chr($temp);
        }
    }
    return $data;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>